#!/usr/bin/env python3
"""
Settings file belonging to generate_grid.py.

"""

__author__ = "Guido Legemaate"
__copyright__ = "Copyright 2016, G.A.G. Legemaate"
__credits__ = ["FDS"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Guido Legemaate"
__email__ = "g.legemaate@brandweeraa.nl"
__status__ = "Prototype"

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# height and width of generated grid (in meters, when using spatial reference 28992, degrees otherwise)
# PLEASE NOTE that the grid generation is not optimized to handle values < 100
WIDTH = 100
HEIGHT = 100

# spatial reference for the generated .prj file, default is 28992 (RD Amersfoort)
SPAT_REF = 28992

# field/column name for the .dbf file, to keep compliancy with the CBS dataset, set to one of either suggested below according to grid size
FIELD = "c28992r100"    # for grid of 100 x 100 meters
# FIELD = "c28992r500"    # for grid of 500 x 500 meters

# folder name (relative to script directory) where generated grid will be placed, don't forget trailing slash
# Linux used forward slashes (/) as folder separators, Windows uses backslashes (\). Set accordingly. 
FOLDER = "output/"