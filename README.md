# GRID generator #

Met dit python script kan een standaard rechthoekig geospatial grid gemaakt worden, vergelijkbaar met het CBS grid.

## Werking ##
De executable (grid_generator) heeft één argument nodig, dit is de bestandsnaam (plus eventueel path) van het input bestand.

### Voorbeeld ###
In de directory 'input' staat een voorbeeld input bestand, de Nederlandse landsgrens als shapefile. Om aan de hand van deze polygoon een grid te genereren doe je het volgende in de script directory:

```
#!Command Line

python3 generate_grid.py input/landsgrens.shp
```
Let op: het script is geschreven in Python versie 3 (niet op elke machine de standaard Python interpreter).

## Features ##
In de settings file kunnen de volgende opties worden ingesteld:

* grootte van x en y afstand zelf te bepalen, standaard 100 meter
* srs-id projectie zelf in te stellen, standaard (RD Amersfoort) 28992
* veldnaam welke wordt weggeschreven naar de .dbf file kan vrij worden gekozen, standaard c28992r100 (CBS default)

## ToDo 8-12-2016 ##
* aantal meters (x), laatste deel van veldnaam c28992r100, automatisch aan laten passen bij wijziging, nu vast op 100 meter
* input van online WFS/WMS laag mogelijk maken, nu is een fysiek lokaal bestand nog nodig