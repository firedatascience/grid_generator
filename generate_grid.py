#!/usr/bin/env python3
"""
Generate a 100 x 100 meters rectangular (CBS) grid based on the extents of a polygon.

Arguments: 
    filename: ESRI shapefile (.shp) with a polygon which's extents will be used for grid generation.
    
Settings:
    See settings.py.
"""

import settings
import sys
import math
import shapefile as shp
from osgeo import osr, ogr
from datetime import datetime

__author__ = "Guido Legemaate"
__copyright__ = "Copyright 2016, G.A.G. Legemaate"
__credits__ = ["FDS"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Guido Legemaate"
__email__ = "g.legemaate@brandweeraa.nl"
__status__ = "Prototype"

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

start_time = datetime.now()

def get_extents(filename):
    """Retrieve the extents of an ESRI shapefile layer."""
    ds = ogr.Open(filename, 0)
    layer = ds.GetLayer()
    x_min, x_max, y_min, y_max = layer.GetExtent()
    return x_min, y_min, x_max, y_max # beware, order changed!

def format_extents(in_minx, in_miny, in_maxx, in_maxy):
    """Convert a float to an integer and round to a hundred meters."""
    minx = int(in_minx / 100) * 100
    miny = int(in_miny / 100) * 100
    maxx = (int(in_maxx / 100) + 1) * 100
    maxy = (int(in_maxy / 100) + 1) * 100
    return minx, miny, maxx, maxy

def generate_grid(width, height, minx, miny, maxx, maxy, fieldname, folder, spatref):
    nx = int(math.ceil(abs(maxx - minx)/width))
    ny = int(math.ceil(abs(maxy - miny)/height))
    nmax = nx * ny
    fname = folder + "poly_grid_" + str(width) + "x" + str(height)
    w = shp.Writer(fname, shapeType=5)
    w.autoBalance = 1
    w.field(fieldname)
    gid = 0
    # the algorithm that creates the actual grid starts here
    for i in range(ny):
        for j in range(nx):
            gid += 1
            min_x = min(minx + width * j, maxx)
            max_y = max(maxy - height * i, miny)
            max_x = min(minx + width * (j + 1), maxx)
            min_y = max(maxy - height * (i + 1), miny)
            vertices = []
            parts = []
            vertices.append([min_x, max_y])
            vertices.append([max_x, max_y])
            vertices.append([max_x, min_y])
            vertices.append([min_x, min_y])
            parts.append(vertices)
            w.poly(parts)
            if len(str(min_x)) < 6:
                e_val = str(min_x)[:3].zfill(4)
            elif len(str(min_x)) >= 6:
                e_val = str(min_x)[:4]       
            if len(str(min_y)) < 6:
                n_val = str(min_y)[:3].zfill(4)
            elif len(str(min_y)) >= 6:
                n_val = str(min_y)[:4]  
            fieldname = "E" + e_val + "N" + n_val
            w.record(fieldname)
            print(str(math.floor((gid * 100) / nmax)) + "% [" + str(gid) + "/" + str(nmax) + "]")
    # end of grid generation
    gridgen_time = datetime.now() - start_time
    print("\n" + str(datetime.now()) + " - RUNTIME grid generation " + str(gridgen_time))
    #print("\n" + str(datetime.now()) + " - INFORMATION now writing grid to disk - this takes, depending on the I/O speed of your system, approximately " + str(2.5 * gridgen_time))
    #temp_time = datetime.now()
    # write grid to disk
    #w.save(fname)
    #print("\n" + str(datetime.now()) + " - RUNTIME grid writing to disk "+ str(datetime.now() - temp_time))
    # add a spatial projection file
    spatialRef = osr.SpatialReference()
    spatialRef.ImportFromEPSG(spatref)
    spatialRef.MorphToESRI()
    fname_ext = fname + ".prj"
    file = open(fname_ext, 'w')
    file.write(spatialRef.ExportToWkt())
    file.close()
    print("\n" + str(datetime.now()) + " - INFORMATION wrote projection file with spatial reference " + str(spatref) + " to " + fname_ext)
    print("\n" + str(datetime.now()) + " - RUNTIME total "+ str(datetime.now() - start_time))
    print("\n" + str(datetime.now()) + " - FINISHED creating the grid, files written to " + fname)

def main(args):
    if (len(args) == 2):
        print("Argument passed (filename):", args[1])
        # get extents from input file
        x_min, y_min, x_max, y_max = get_extents(args[1])
        # format extents and convert to integer
        minx, miny, maxx, maxy = format_extents(x_min, y_min, x_max, y_max)
        # get parameters for grid from settings file
        width = settings.WIDTH
        height = settings.HEIGHT
        fieldname = settings.FIELD
        folder = settings.FOLDER
        spatref = settings.SPAT_REF
        # generate grid
        generate_grid(width, height, minx, miny, maxx, maxy, fieldname, folder, spatref)
    else:
        print("Foutief argument (aantal).")

if __name__ == '__main__':
    main(sys.argv)